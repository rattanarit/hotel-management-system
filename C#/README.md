# Hotel Management System Version C#

## วิธีการ Run

1. เปิด terminal 
2. เข้าไปใน folder > `C#/Hotel Management System/HotelManagementSystem`
3. ใช้คำสั่ง dotnet run {file input path}

\*\*\*หมายเหตุ : หากไม่ใส่ {file input path} จะใช้ค่า default ที่อยู่ใน file `launchSettings.json`