using System;
using System.IO;
using System.IO.Abstractions;
using System.Text;
using HotelManagementSystem.Shared.Services;
using Microsoft.Extensions.Configuration;
using NSubstitute;
using Xunit;

namespace HotelManagementSystem.Test.Services;

public class HotelServiceTest
{
    private readonly string json =  "{ \"FileInput\": \"input.txt\" }";
    private readonly string path = "../../../../../../";
    private readonly IHotelService _hotelService;
    private readonly IFileSystem _fileSystem;

    public HotelServiceTest()
    {
        IConfiguration configuration = new ConfigurationBuilder()
            .AddJsonStream(new MemoryStream(Encoding.ASCII.GetBytes(json))).Build();
        _fileSystem = Substitute.For<IFileSystem>();
        _hotelService = new HotelService(configuration, _fileSystem);
    }
    
    [Fact]
    public void HotelService_Run_Should_Correct_Output()
    {
        // Arrange
        var stringWriter = new StringWriter();
        Console.SetOut(stringWriter);
        _fileSystem.File.Exists(Arg.Any<string>()).Returns(true);
        _fileSystem.File.ReadAllLines("input.txt").Returns(File.ReadAllLines(path+"input.txt"));
        var output = File.ReadAllText(path+"output.txt")+"\n";
        // Act
        _hotelService.Run();
        // Assert
        Assert.Equal(stringWriter.ToString(), output);
    }
}