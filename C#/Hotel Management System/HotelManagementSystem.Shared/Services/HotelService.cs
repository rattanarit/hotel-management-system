using System.IO.Abstractions;
using HotelManagementSystem.Shared.Models;
using Microsoft.Extensions.Configuration;

namespace HotelManagementSystem.Shared.Services;

public class HotelService : IHotelService
{
    private readonly string _filePath;
    private readonly IFileSystem _fileSystem;
    private HotelModel? _hotel;
    public HotelService(IConfiguration configuration,IFileSystem fileSystem)
    {
        _fileSystem = fileSystem;
        _filePath = configuration["FileInput"];
    }
    public void Run()
    {
        var inputs = _fileSystem.File.ReadAllLines(_filePath);
        foreach (var input in inputs)
        {
            ProcessCommand(new CommandModel(input));
        }

    }
    private void ProcessCommand(CommandModel commandModel)
    {
        switch (commandModel.Command)
        {
            case "create_hotel":
                CreateHotel(commandModel.Parameters);
                break;
            case "book":
                BookRoom(commandModel.Parameters);
                break;
            case "list_available_rooms":
                ListAvaliableRooms();
                break;
            case "checkout":
                Checkout(commandModel.Parameters);
                break;
            case "list_guest":
                ListGuests();
                break;
            case "get_guest_in_room":
                GetGuestInRoom(commandModel.Parameters);
                break;
            case "list_guest_by_age":
                ListGuestsByAge(commandModel.Parameters);
                break;
            case "list_guest_by_floor":
                ListGuestsByFloor(commandModel.Parameters);
                break;
            case "checkout_guest_by_floor":
                CheckoutGuestByFloor(commandModel.Parameters);
                break;
            case "book_by_floor":
                BookByFloor(commandModel.Parameters);
                break;
        }
    }
    
    private void CreateHotel(string[] parameters)
    {
        if (_hotel != null)
            throw new InvalidOperationException("Hotel already exists");
        
        try
        {
            int floors = int.Parse(parameters[0]);
            int rooms = int.Parse(parameters[1]);
            _hotel = new HotelModel(floors, rooms);
            Console.WriteLine("Hotel created with {0} floor(s), {1} room(s) per floor.", floors, rooms);
        }
        catch (Exception)
        {
            throw new ArgumentException("Invalid parameters");
        }

    }

    private void BookRoom(string[] parameters)
    {
        if (_hotel == null)
            throw new InvalidOperationException("Hotel does not exist");

        try
        {
            int floor = int.Parse(parameters[0].Substring(0, 1));
            int room = int.Parse(parameters[0].Substring(1,2));
            string name = parameters[1], age = parameters[2];
            var hotelRoom = _hotel.GetRoom(floor, room);
            if (!hotelRoom.IsAvailable())
            {
                Console.WriteLine("Cannot book room {0} for {1}, The room is currently booked by {2}."
                    ,hotelRoom.RoomNumber, name, hotelRoom.Guest?.Name);
            }
            else
            {
                var guest = new GuestModel(name, int.Parse(age));
                _hotel.AddGuest(hotelRoom,guest);
                _hotel.AddKeyCard(hotelRoom);
                Console.WriteLine("Room {0} is booked by {1} with keycard number {2}.", hotelRoom.RoomNumber, guest.Name, hotelRoom.KeyCardNumber);
            }
        }
        catch (Exception)
        {
            throw new ArgumentException("Invalid parameters");
        }

    }

    private void ListAvaliableRooms()
    {
        if (_hotel == null)
            throw new InvalidOperationException("Hotel does not exist");
        var floorAndRooms = _hotel.FloorAndRooms
            .SelectMany(floor => floor.Where(room => room.IsAvailable()))
            .ToList();
        Console.WriteLine(string.Join(",",floorAndRooms.Select(e=>e.RoomNumber)));
    }
    
    private void Checkout(string[] parameters)
    {
        if (_hotel == null)
            throw new InvalidOperationException("Hotel does not exist");
        try
        {
            int key = int.Parse(parameters[0]);
            string name = parameters[1];
            var hotelRoom = _hotel.GetRoomByKeyCard(key);
            if (hotelRoom.Guest?.Name != name)
                Console.WriteLine("Only {0} can checkout with keycard number {1}."
                , hotelRoom.Guest?.Name, hotelRoom.KeyCardNumber);
            else
            {
                _hotel.RemoveGuest(hotelRoom);
                _hotel.RemoveKeyCard(hotelRoom);
                Console.WriteLine("Room {0} is checkout.", hotelRoom.RoomNumber);
            }
        }
        catch (Exception)
        {
            throw new ArgumentException("Invalid parameters");
        }
    }
    
    private void ListGuests()
    {
        if (_hotel == null)
            throw new InvalidOperationException("Hotel does not exist");
        var guests = _hotel.GuestNames.Select(e => e.Key).ToList();
        Console.WriteLine(string.Join(", ",guests));
    }
    
    private void GetGuestInRoom(string[] parameters)
    {
        if (_hotel == null)
            throw new InvalidOperationException("Hotel does not exist");
        try
        {
            int floor = int.Parse(parameters[0].Substring(0, 1));
            int room = int.Parse(parameters[0].Substring(1, 2));
            Console.WriteLine(_hotel.GetRoom(floor, room).Guest?.Name);
        }
        catch (Exception)
        {
            throw new ArgumentException("Invalid parameters");
        }
    }
    
    private void ListGuestsByAge(string[] parameters)
    {
        if (_hotel == null)
            throw new InvalidOperationException("Hotel does not exist");
        try
        {
            string operatorString = parameters[0];
            int age = int.Parse(parameters[1]);
            var guests = _hotel.FloorAndRooms
                .SelectMany(floor => floor.Where(room => room.Guest != null))
                .Where(room => operatorString switch
                {
                    ">" => room.Guest!.Age > age,
                    "<" => room.Guest!.Age < age   ,
                    "=" => room.Guest!.Age == age,
                    _ => throw new ArgumentException("Invalid operator")
                })
                .Select(e => e.Guest!.Name)
                .ToList();
            Console.WriteLine(string.Join(", ", guests));
        }
        catch (Exception)
        {
            throw new ArgumentException("Invalid parameters");
        }
    }

    private void ListGuestsByFloor(string[] parameters)
    {
        if (_hotel == null)
            throw new InvalidOperationException("Hotel does not exist");
        try
        {
            int floor = int.Parse(parameters[0])-1;
            var guests = _hotel.FloorAndRooms[floor]
                .Where(room => room.Guest != null)
                .Select(e => e.Guest!.Name)
                .ToList();
            Console.WriteLine(string.Join(", ", guests));
        }
        catch (Exception)
        {
            throw new ArgumentException("Invalid parameters");
        }
    }

    private void CheckoutGuestByFloor(string[] parameters)
    {
        if (_hotel == null)
            throw new InvalidOperationException("Hotel does not exist");
        try
        {
            int floor = int.Parse(parameters[0])-1;
            var rooms = _hotel.FloorAndRooms[floor]
                .Where(room => room.Guest != null)
                .ToList();
            foreach (var room in rooms)
            {
                _hotel.RemoveGuest(room);
                _hotel.RemoveKeyCard(room);
            }
            Console.WriteLine("Room {0} are checkout.", 
                string.Join(", ", rooms.Select(e=>e.RoomNumber)));
        }
        catch (Exception)
        {
            throw new ArgumentException("Invalid parameters");
        }
    }

    private void BookByFloor(string[] parameters)
    {
        if (_hotel == null)
            throw new InvalidOperationException("Hotel does not exist");
        try
        {
            int floor = int.Parse(parameters[0]);
            string name = parameters[1], age = parameters[2];
            var rooms = _hotel.FloorAndRooms[floor-1]
                .Where(room => room.IsAvailable())
                .ToList();
            if (rooms.Count != _hotel.FloorAndRooms[0].Length)
            {
                Console.WriteLine("Cannot book floor {0} for {1}.", floor, name);
            }
            else
            {
                var guest = new GuestModel(name, int.Parse(age));
                foreach (var room in rooms)
                {
                    _hotel.AddGuest(room, guest);
                    _hotel.AddKeyCard(room);
                }
                Console.WriteLine("Room {0} are booked with keycard number {1}", 
                    string.Join(", ", rooms.Select(e=>e.RoomNumber)), 
                    string.Join(", ", rooms.Select(e=>e.KeyCardNumber)));
            }
        }
        catch (Exception)
        {
            throw new ArgumentException("Invalid parameters");
        }
    }
}