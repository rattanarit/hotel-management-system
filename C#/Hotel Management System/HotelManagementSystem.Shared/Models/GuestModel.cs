namespace HotelManagementSystem.Shared.Models;

public class GuestModel
{
    public string Name { get; }
    public int Age { get; }
    
    public GuestModel(string name, int age)
    {
        Name = name;
        Age = age;
    }
}