namespace HotelManagementSystem.Shared.Models;

public class HotelModel
{
    public RoomModel[][] FloorAndRooms { get; }
    private int[] Keys { get; }
    public Dictionary<string, int> GuestNames { get; }
    public Dictionary<int, string> KeyToRooms { get; }
    public HotelModel(int floor, int rooms)
    {
        Keys = new int[floor*rooms];
        KeyToRooms = new Dictionary<int, string>();
        GuestNames = new Dictionary<string, int>();

        FloorAndRooms = new RoomModel[floor][];
        for (int i = 0; i < floor; i++)
        {
            FloorAndRooms[i] = new RoomModel[rooms];
            for (int j = 0; j < rooms; j++)
            {
                FloorAndRooms[i][j] = new RoomModel(i+1 + (j+1).ToString("00"));
            }
        }
    }
    
    public RoomModel GetRoom(int floor, int room)
    {
        return FloorAndRooms[floor-1][room-1];
    }
    public RoomModel GetRoomByKeyCard(int key)
    {
        int floor = int.Parse(KeyToRooms[key].Substring(0, 1));
        int room = int.Parse(KeyToRooms[key].Substring(1, 2));
        return FloorAndRooms[floor-1][room-1];
    }
    public void AddKeyCard(RoomModel room)
    {
        room.KeyCardNumber = GetKeyCard();
        Keys[room.KeyCardNumber-1] = 1;
        KeyToRooms.Add(room.KeyCardNumber, room.RoomNumber);
    }
    public void RemoveKeyCard(RoomModel room)
    {
        Keys[room.KeyCardNumber-1] = 0;
        KeyToRooms.Remove(room.KeyCardNumber);
    }
    public void AddGuest(RoomModel room, GuestModel guest)
    {
        room.Guest = guest;
        int count;
        GuestNames.TryGetValue(guest.Name, out count);
        GuestNames[guest.Name] = count+1;
    }
    public void RemoveGuest(RoomModel room)
    {
        int count;
        GuestNames.TryGetValue(room.Guest!.Name, out count);
        GuestNames[room.Guest!.Name] = count-1;
        if(count == 1)
        {
            GuestNames.Remove(room.Guest!.Name);
        }
        room.Guest = null;
    }

    private int GetKeyCard()
    {
        return Array.IndexOf(Keys, 0) + 1;

    }
}