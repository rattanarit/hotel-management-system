namespace HotelManagementSystem.Shared.Models;

public class RoomModel
{
    public GuestModel? Guest { get; set; }
    public string RoomNumber { get; }
    public int KeyCardNumber { get; set; }

    public RoomModel(string roomNumber)
    {
        RoomNumber = roomNumber;
    }
    
    public bool IsAvailable()
    {
        return Guest == null;
    }
}