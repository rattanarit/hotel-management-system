namespace HotelManagementSystem.Shared.Models;

public class CommandModel
{
    public string Command { get; }
    public string[] Parameters { get; }

    public CommandModel(string line)
    {
        string[] split = line.Split(' ');
        Command = split[0];
        Parameters = split.Skip(1).ToArray();
    }
}