﻿using System.IO.Abstractions;
using HotelManagementSystem.Shared.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;


namespace Hotel_Management_System;

static class Program
{
    private static void Main(string[] args)
    {
        if(args.Length == 1)
        {
            Environment.SetEnvironmentVariable("FileInput", args[0]);
        }
        Run(GetConfiguration());
    }
    private static IConfigurationRoot GetConfiguration()
    {
        return new ConfigurationBuilder()
            .AddEnvironmentVariables()
            .Build();
    }

    private static void Run(IConfiguration configuration)
    {
        var serviceCollection = new ServiceCollection();
        serviceCollection.AddSingleton(configuration);
        serviceCollection.AddScoped<IHotelService, HotelService>();
        serviceCollection.AddScoped<IFileSystem, FileSystem>();
        
        serviceCollection
            .BuildServiceProvider()
            .GetService<IHotelService>()
            ?.Run(); 
    }
}